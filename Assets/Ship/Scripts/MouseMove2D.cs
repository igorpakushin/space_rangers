﻿using UnityEngine;
using System.Collections;

public class MouseMove2D : MonoBehaviour
{

    private Vector3 mousePosition;
    public float accSpeed = 1.6f;
    public float maxTorque = 0.4f;
    public float maxSpeed = 125.0f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButton(1))
        {
            // Create a ray going into the scene starting 
            // from the screen position provided 
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            // RaycastHit hit;
            //if( Physics.Raycast( ray, out hit ) )
            //	hitPoint = hit.point;

            // intersection point between the ray and 
            // the Y=0 plane (horizontal plane)
            float t = -ray.origin.y / ray.direction.y;
            Vector3 hitPoint = ray.GetPoint(t);
            hitPoint.y = transform.position.y;
            //Debug.Log(hitPoint);

            Vector3 targetDir = hitPoint - transform.position;
            float angle = Vector3.Angle(targetDir, transform.forward);
            Vector3 rotated = Vector3.RotateTowards(transform.forward, targetDir, angle, 0);
            if (Vector3.Cross(targetDir, transform.forward).y > 0)
                angle = -angle;

            if (angle != 0)
            {
                float unitTorque = angle / Mathf.Abs(angle);
                GetComponent<Rigidbody>().AddTorque(new Vector3(0, unitTorque * maxTorque, 0));
            }

            GetComponent<Rigidbody>().AddForce(transform.forward * accSpeed);
            float speed = Vector3.Distance(GetComponent<Rigidbody>().velocity, Vector3.zero);

            Debug.DrawRay(transform.position, rotated * maxSpeed);

            if (speed > maxSpeed)
            {
                GetComponent<Rigidbody>().velocity = transform.forward * maxSpeed;
            }
        }
    }
}
